Rails.application.routes.draw do
  resources :physician_reports

  resources :patient_billings

  devise_for :users
  # You can have the root of your site routed with "root"
  root 'welcome#index'
  get 'welcome/index'
  get 'physician_reports/index'
  get 'patient_billings/index'
  get 'users/sign_in'
  get 'users/sign_out'
  get 'appointments/index'
  get 'appointments/new'
  get 'patients/index'
  get 'patients/new'
  get 'physicians/index'
  get 'physicians/new'
  get 'codes/index'
  get 'codes/new'


  resources :appointments
  resources :codes
  resources :physicians
  resources :patients
  resources :users
  resources :physician_reports
  resources :patient_billings


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
