class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.datetime :date
      t.string :reason
      t.string :note
      t.integer :patient_id
      t.integer :physician_id
      t.integer :code_id

      t.timestamps
    end
  end
end
