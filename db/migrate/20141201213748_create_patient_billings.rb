class CreatePatientBillings < ActiveRecord::Migration
  def change
    create_table :patient_billings do |t|
      t.integer :appointment_id

      t.timestamps
    end
  end
end
