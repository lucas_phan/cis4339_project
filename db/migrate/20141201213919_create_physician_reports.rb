class CreatePhysicianReports < ActiveRecord::Migration
  def change
    create_table :physician_reports do |t|
      t.integer :appointment_id

      t.timestamps
    end
  end
end
