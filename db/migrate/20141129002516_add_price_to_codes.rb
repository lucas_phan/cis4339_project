class AddPriceToCodes < ActiveRecord::Migration
  def change
    add_column :codes, :price, :float
  end
end
