class RemoveDescriptionFromCodes < ActiveRecord::Migration
  def change
    remove_column :codes, :description, :string
  end
end
