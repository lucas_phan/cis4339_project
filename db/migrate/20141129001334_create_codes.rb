class CreateCodes < ActiveRecord::Migration
  def change
    create_table :codes do |t|
      t.string :num
      t.string :description

      t.timestamps
    end
  end
end
