json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :date, :reason, :note, :patient_id, :physician_id, :code_id
  json.url appointment_url(appointment, format: :json)
end
