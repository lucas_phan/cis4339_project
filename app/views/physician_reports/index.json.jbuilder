json.array!(@physician_reports) do |physician_report|
  json.extract! physician_report, :id, :appointment_id
  json.url physician_report_url(physician_report, format: :json)
end
