json.array!(@patient_billings) do |patient_billing|
  json.extract! patient_billing, :id, :appointment_id
  json.url patient_billing_url(patient_billing, format: :json)
end
