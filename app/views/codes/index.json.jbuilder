json.array!(@codes) do |code|
  json.extract! code, :id, :num, :description
  json.url code_url(code, format: :json)
end
