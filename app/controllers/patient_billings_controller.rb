class PatientBillingsController < ApplicationController
  before_action :set_patient_billing, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource except: [:create]

  respond_to :html

  def index
    @patient_billings = PatientBilling.all
    respond_with(@patient_billings)
  end

  def show
    respond_with(@patient_billing)
  end

  def new
    @patient_billing = PatientBilling.new
    respond_with(@patient_billing)
  end

  def edit
  end

  def create
    @patient_billing = PatientBilling.new(patient_billing_params)
    @patient_billing.save
    respond_with(@patient_billing)
  end

  def update
    @patient_billing.update(patient_billing_params)
    respond_with(@patient_billing)
  end

  def destroy
    @patient_billing.destroy
    respond_with(@patient_billing)
  end

  private
    def set_patient_billing
      @patient_billing = PatientBilling.find(params[:id])
    end

    def patient_billing_params
      params.require(:patient_billing).permit(:appointment_id)
    end
end
