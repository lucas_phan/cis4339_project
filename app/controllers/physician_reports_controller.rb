class PhysicianReportsController < ApplicationController
  before_action :set_physician_report, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  load_and_authorize_resource except: [:create]

  respond_to :html

  def index
    @physician_reports = PhysicianReport.all
    respond_with(@physician_reports)
  end

  def show
    respond_with(@physician_report)
  end

  def new
    @physician_report = PhysicianReport.new
    respond_with(@physician_report)
  end

  def edit
  end

  def create
    @physician_report = PhysicianReport.new(physician_report_params)
    @physician_report.save
    respond_with(@physician_report)
  end

  def update
    @physician_report.update(physician_report_params)
    respond_with(@physician_report)
  end

  def destroy
    @physician_report.destroy
    respond_with(@physician_report)
  end

  private
    def set_physician_report
      @physician_report = PhysicianReport.find(params[:id])
    end

    def physician_report_params
      params.require(:physician_report).permit(:appointment_id)
    end
end
