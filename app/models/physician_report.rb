class PhysicianReport < ActiveRecord::Base
  belongs_to :appointment
  has_many :codes, through: :appointment
  has_many :physicians, through: :appointment
end
