class PatientBilling < ActiveRecord::Base
  belongs_to :appointment
  has_many :codes, through: :appointment
  has_many :patients, through: :appointment
end
