class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :codes, through: :appointments
  has_many :patients, through: :appointments
  has_many :physician_reports, through: :appointments
end
