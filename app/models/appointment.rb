class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :code
  has_many :patient_billings
  has_many :physician_reports
end
