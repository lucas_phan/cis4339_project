class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :codes, through: :appointments
  has_many :patient_billings, through: :appointments
end
