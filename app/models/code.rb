class Code < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :patients, through: :appointments
  has_one :patient_billing, through: :appointment
end
