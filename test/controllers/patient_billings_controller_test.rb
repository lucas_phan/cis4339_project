require 'test_helper'

class PatientBillingsControllerTest < ActionController::TestCase
  setup do
    @patient_billing = patient_billings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patient_billings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create patient_billing" do
    assert_difference('PatientBilling.count') do
      post :create, patient_billing: { appointment_id: @patient_billing.appointment_id }
    end

    assert_redirected_to patient_billing_path(assigns(:patient_billing))
  end

  test "should show patient_billing" do
    get :show, id: @patient_billing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @patient_billing
    assert_response :success
  end

  test "should update patient_billing" do
    patch :update, id: @patient_billing, patient_billing: { appointment_id: @patient_billing.appointment_id }
    assert_redirected_to patient_billing_path(assigns(:patient_billing))
  end

  test "should destroy patient_billing" do
    assert_difference('PatientBilling.count', -1) do
      delete :destroy, id: @patient_billing
    end

    assert_redirected_to patient_billings_path
  end
end
