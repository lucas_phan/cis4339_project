require 'test_helper'

class PhysicianReportsControllerTest < ActionController::TestCase
  setup do
    @physician_report = physician_reports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:physician_reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create physician_report" do
    assert_difference('PhysicianReport.count') do
      post :create, physician_report: { appointment_id: @physician_report.appointment_id }
    end

    assert_redirected_to physician_report_path(assigns(:physician_report))
  end

  test "should show physician_report" do
    get :show, id: @physician_report
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @physician_report
    assert_response :success
  end

  test "should update physician_report" do
    patch :update, id: @physician_report, physician_report: { appointment_id: @physician_report.appointment_id }
    assert_redirected_to physician_report_path(assigns(:physician_report))
  end

  test "should destroy physician_report" do
    assert_difference('PhysicianReport.count', -1) do
      delete :destroy, id: @physician_report
    end

    assert_redirected_to physician_reports_path
  end
end
